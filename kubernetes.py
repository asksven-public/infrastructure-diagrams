from diagrams import Cluster, Diagram, Node, Edge
from diagrams.k8s.compute import Pod
from diagrams.k8s.compute import Deployment
from diagrams.onprem.database import MongoDB
from diagrams.k8s.group import Namespace
from diagrams.k8s.network import Ing
from diagrams.gcp.network import LoadBalancing
from diagrams.onprem.network import Nginx
from diagrams.programming.language import Csharp
from diagrams.onprem.client import Users
from diagrams.onprem.auth import Oauth2Proxy
from diagrams.k8s.network import Netpol
from diagrams.onprem.logging import Loki
from diagrams.onprem.monitoring import Prometheus
from diagrams.azure.database import BlobStorage
from diagrams.azure.security import KeyVaults
from diagrams.onprem.security import Vault
from diagrams.programming.framework import Vue

with Diagram("Application Architecture", show=False):

    #gcp_lb = LoadBalancing("GCP LB")
    auth = Oauth2Proxy("IDP")
    i_user = Users("Internal users")
    e_user = Users("external users")


    with Cluster("Kubernetes"):

        with Cluster("Logging and Monitoring"):
            logging = Loki("Logs")
            metrics = Prometheus("Metrics")

        with Cluster("Nginx ingress controller"):
            nginx= Nginx("")

        with Cluster("MyApp namespace"):
            myapp_ing = Ing("")
            with Cluster("App"):
                myapp_frontend = Vue("Frontend")
                myapp_backend = Csharp("Backend")

            with Cluster("MongoDB"):
                myapp_db = MongoDB("myapp-db")
            myapp_netpol = Netpol()    
    with Cluster("Encrypted Storage"):
        storage=BlobStorage()
        kms=KeyVaults()
    external_kms=Vault("External Key Management")


    i_user >> Edge(label="https") >> nginx
    i_user >> Edge(style="dashed", label="oauth") >> auth >> Edge(style="dashed") >> i_user
    
    
    e_user >> Edge(label="https") >> nginx
    e_user >> Edge(style="dashed", label="oauth") >> auth >> Edge(style="dashed") >> e_user
    nginx >> Edge(label="http") >> myapp_ing >> Edge(label="http") >> myapp_backend >> Edge(label="tcp") >> myapp_db
    
    nginx >> Edge(style="dashed") >> metrics
    nginx >> Edge(style="dashed") >> logging
    myapp_backend >> Edge(style="dashed") >> metrics
    myapp_backend >> Edge(style="dashed") >> logging
    myapp_db >> Edge(style="dashed") >> metrics
    myapp_db >> Edge(style="dashed") >> logging
    myapp_db >> Edge() >> storage
    kms >> external_kms
    storage >> kms
    