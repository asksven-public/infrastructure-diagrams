# Infrastructure diagrams

[Diagram](https://diagrams.mingrammer.com/docs/getting-started/installation) is a neat tool to generate diagrams from code.

## Locally

1. Create a python venv (once): `python -m venv venv`
1. Activate it: `source venv/bin/activate`
1. Install diagrams: `pip install diagrams`
1. Install graphviz: `brew install graphviz`
1. Run: `python kubernetes.py` to generate `application_architecture.png`

## Docker

1. Build: `docker build . -t diagrams`
1. Run : `docker run -it -v $(pwd)/:/test -w /test diagrams python kubernetes.py`

## Result

![Architecture diagram](application_architecture.png)